import React, { useEffect, useMemo, useState } from 'react'
import { useRouter } from 'next/router'

const AuthContext = React.createContext()
const { Provider } = AuthContext

const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(null)

  const setUserAuthInfo = (authToken) => {
    localStorage.setItem('token', authToken)

    setToken(authToken)
  }

  useEffect(() => {
    const authToken = localStorage.getItem('token')

    setToken(authToken)
  }, [])

  // checks if the user is authenticated or not
  const isUserAuthenticated = () => {
    const token = localStorage.getItem('token')
    return !!token
  }

  return (
    <Provider
      value={{
        token,
        setAuthState: (userAuthInfo) => setUserAuthInfo(userAuthInfo),
        isUserAuthenticated,
      }}
    >
      {children}
    </Provider>
  )
}

export { AuthContext, AuthProvider }
