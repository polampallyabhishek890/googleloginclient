import { useRouter } from 'next/router'
import { useEffect, useContext } from 'react'
import { useQuery } from 'react-query'
import { AuthContext } from '../context/auth-context'
import axios from '../axios'

export default function useAuthenticated() {
  const router = useRouter()

  const { isUserAuthenticated, setAuthState } = useContext(AuthContext)

  const logout = () => {
    setAuthState(null)
    router.push('/login')
  }

  const { isLoading } = useQuery(
    ['verify-token'],
    () => {
      return axios.get('/auth/verify-login')
    },
    {
      onError: logout,
      retry: false,
    },
  )

  useEffect(() => {
    if (!isLoading) {
      if (!isUserAuthenticated()) {
        router.push('/login')
      }
    }
  }, [isUserAuthenticated, router, isLoading])

  return { isLoading, logout }
}
