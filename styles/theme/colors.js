const colors = {
  primary: {
    900: '#2E126D',
    800: '#7F59BC',
    700: '#845EC2',
    600: '#9662F0',
    500: '#AD83F7',
    400: '#CCB0FF',
    300: '#DCC9FF',
    200: '#F9CBFF',
    100: '#FFE4FF',
    50: '#F6F1FF',
  },
}

export default colors
