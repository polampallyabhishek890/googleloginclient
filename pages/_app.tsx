import type { AppProps } from 'next/app'
import { ChakraProvider } from '@chakra-ui/react'
import { QueryClientProvider, QueryClient } from 'react-query'
import { GoogleOAuthProvider } from '@react-oauth/google'

import '../styles/globals.css'
import { AuthProvider } from '../context/auth-context'
import theme from '../styles/theme'

const client = new QueryClient()

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <GoogleOAuthProvider clientId="666217312897-qbft1eo9mil1t8ept3r7024ado2h8f59.apps.googleusercontent.com">
      <AuthProvider>
        <QueryClientProvider client={client}>
          <ChakraProvider theme={theme}>
            <Component {...pageProps} />
          </ChakraProvider>
        </QueryClientProvider>
      </AuthProvider>
    </GoogleOAuthProvider>
  )
}

export default MyApp
