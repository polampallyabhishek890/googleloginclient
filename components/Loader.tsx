import { Center, Spinner } from '@chakra-ui/react'
import React from 'react'

export default function Loader() {
  return (
    <Center
      position={'fixed'}
      top={'0'}
      left={'0'}
      w={'full'}
      h={'full'}
      bg={'rgba(0,0,0,0.1)'}
    >
      <Spinner size={'md'} zIndex={2} />
    </Center>
  )
}
